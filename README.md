# Presentation
NeoBib \[ne.o.bɪb\] is a CLI bibliography management program to use with LaTeX.

# Installing
Just copy *neobib.py* to whatever path (system or not) you want to execute it from.

The following command (ran with root privileges) should work under Linux:
```sh
cp neobib.py /usr/bin/neobib && chmod a+x /usr/bin/neobib
```

# Usage
## Configuration
NeoBib's configuration is stored in a TOML file called *.nbb\_cfg.toml* (sample provided). When launched, NeoBib will search for this file in the current directory and, if not found, in the user's home directory.

In this file, you can set the following options:
- text_edit: command to launch your favorite lightweight text editor
- pdf_view: command to launch your favorite pdf viewer
- web_b: command to launch your favorite web browser
- type_order: a list of BibTeX reference types in the order you want them to be sorted
- key_order: a list of NeoBib reference properties (~BibTeX properties, see *format.md*) in the order you want them to be sorted
- bib_order: a list of NeoBib reference properties you want to be put in generated .bib files (in order)
- short_order: a list of NeoBib reference properties you want to be put in generated .bib files when under length constraint (in order)
- search_keys: a list of NeoBib reference properties to search within when using NeoBib's reference search function
- d_db: path to the default database
- f_path: path to the place where .pdf files of references are stored

## Database
NeoBib relies on a bibliographic database in a text-based format described in *format.md*. You can write it with a simple text editor. Some NeoBib commands allow to add or edit a single reference in an external text editor.

When using NeoBib, you may pass the database as parameter \[BASE\] or use `_` to use the default database (ensure you have correctly setup its path).

## LaTeX
NeoBib generates BibTeX files you can use with LaTeX.

The *fields* (see *format.md*) present in you database may use BibTeX syntax, notably for accentuated characters but, these will not be processed by NeoBib, which may alter functions like search. It is recommended to use Unicode characters rather than LaTeX commands and a modern LaTeX implementation, like LuaLaTeX or XeLaTeX with Biber, to have good Unicode support.

In your database, all references have a *code* (see *format.md*). To cite these references in LaTeX, use this code (`\cite{CODE}`).

## Commands
```sh
neobib prod [BASE] [TEX]* [OUT]
```
Create a BibTeX file from the database. If the optional parameter \[TEX\] is given, only references used in \[TEX\] (.tex file) will be written. Multiple .tex files may be given.

```sh
neobib short [BASE] [TEX]* [OUT]
```
Create a BibTeX file from the database. If the optional parameter \[TEX\] is given, only references used in \[TEX\] (.tex file) will be written. The references will be presented in a short format for situations where length is a constraint. Multiple .tex files may be given.

```sh
neobib regen [BASE]+ [OUT]?
```
Take all references in \[BASE\] files, deal with duplicates, short things, generate codes and output a new database. If \[OUT\] is not given, \[BASE\] is used. When parsing arguments, \[OUT\] has higher priority than \[BASEn\].

```sh
neobib add [BASE] [OUT]?
```
Open your favorite lightweight text editor, allowing you to write a new entry to add to your database. If \[OUT\] is given, a new database will be produced.

```sh
neobib del [BASE] [REF] [OUT]?
```
Delete reference \[REF\]. \[REF\] should be a code of (part of) a title. If multiple matches are found, you will be prompted to select one. If \[OUT\] is given, a new database will be produced.

```sh
neobib edit [BASE] [REF] [OUT]?
```
Open your favorite lightweight text editor, allowing you to edit reference \[REF\]. If \[OUT\] is given, a new database will be produced.

```sh
neobib view [BASE] [REF]
```
Open your favorite lightweight text editor to see \[REF\]'s entry (changes discarded). \[REF\] should be a code or (part of) a title. If multiple matches are found, you will be prompted to select one.

```sh
neobib read [BASE] [REF] [PATH]?
```
Open your favorite pdf reader to read reference \[REF\]. \[PATH\] can be given if the file is not stored in your default .pdf path.

```sh
neobib web [BASE] [REF]
```
Open your favorite web browser visit reference \[REF\]'s web page.

```sh
neobib find [BASE] [REF]
```
Search for reference \[REF\] in the database and print the results. \[REF\] must be a code.

```sh
neobib pick [BASE] [TEX]* [PATH]? [OUT]
```
Take all .pdf files of references in the database used in \[TEX\] (all if no .tex file is given) and copy them to \[OUT\]. \[PATH\] can be given if the file is not stored in your default .pdf path. Multiple .tex files may be given.

```sh
neobib search [BASE] [MODE]? [VAL]
```
Search \[VAL\] in the database and print the results. \[VAL\] may be a single word or a " enclosed string, in which case all terms must match.
The optional \[MODE\] parameter can take the following values (default: `+`).
- \*: search in all fields (in *key_order*)
- +: search in usually searched fields  (in *search_keys*)
- a string of several characters, each defining a field to search in:
- a: Authors
- b: Booktitle
- c: Code
- e: Advisors
- f: File
- j: Journal
- k: Keywords
- l: Link
- n: Note
- r: Abstract
- s: School
- t: Title
- u: Type
- y: Year

```sh
neobib search1 [BASE] [MODE]? [VAL]
```
Same as *search* but only one term of \[VAL\] must match.

# License
This project is placed under protection of the GNU General Public License version 3 or later as published by the Free Software Foundation.

## Contributions
Contributions are welcome. By contributing to this project, you accept that your contribution is GPLv3+.

# Support
This project is in (early) **beta** state.

This project is currently maintained by a single developer on free time with limited testing possibilities; do not expect professional-grade support. You can fill issues but there is no warranty that they will be addressed. Please try to make reports as good as possible (information to reproduce is mandatory, providing a tested fix is great).

All recent POSIX compliant systems should be supported. Non-POSIX-compliant systems, including MS Windows, are **not** officially supported. Non-FOSS POSIX-compliant systems, including macOS, will **not** receive any special attention a GNU/Linux or *BSD system could expect (no fix for macOS-only issues).

## Dependencies
- Python ⩾3.7
- TOML Kit ⩾0.7

# Code of Conduct
Please follow the [KDE Community Code of Conduct](https://kde.org/code-of-conduct/)

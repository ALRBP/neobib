#!/usr/bin/env python3
import sys
import string
import re
import io
import os
import subprocess
import shutil
import tempfile
import tomlkit


TAB_VAL = 4
EQ_LVL = 4
TMP_FILE = os.path.join(tempfile.gettempdir(),"neobib-edit.nbb")
CFG_FILE = ".nbb_cfg.toml"

TYPE_ORDER = ["unpublished","article","inproceedings","conference","incollection","book","inbook","techreport","phdthesis","mastersthesis","manual","booklet","software","misc"]
KEY_ORDER = ["Title","Type","Authors","Advisors","Journal","Booktitle","Edition","Editor","Publisher","Series","Volume","Number","Pages","School","Year","Month","Day","Abstract","Link","File","Keywords","Note"]
BIB_ORDER = ["Title","Authors","Journal","Booktitle","Edition","Series","Volume","Number","Pages","School","Year"]
SHORT_ORDER = ["Title","Authors","Journal","Booktitle","School","Year"]
SEARCH_KEYS = ["Title","Keywords","Abstract","Authors","Code"]
EDIT_CMD = "kwrite"
PDF_CMD = "okular"
WEB_CMD = "falkon"
DEF_DB = "base.nbb"
DEF_PATH = "./"


def substring(a,b):
    return (b.lower().find(a.lower()) > -1)

def searchOne(a,b):
    la = a.split()
    f = 0
    for e in la:
        if substring(e,b):
            f+=1
    return f

def searchAll(a,b):
    la = a.split()
    for e in la:
        if not substring(e,b):
            return False
    return True

def prepSearch(e,l):
    ans = ""
    for i in l:
        if i in e:
            if i == "Authors":
                for a in e["Authors"]:
                    ans+= ' ' + a[0] + ' ' + a[1]
            elif i == "Advisors":
                for a in e["Advisoss"]:
                    ans+= ' ' + a[0] + ' ' + a[1]
            elif i == "Keywords":
                for k in e["Keywords"]:
                    ans+= ' ' + k
            elif i == "Journal":
                ans+= ' ' + e[i][0]
                if len(e[i]) > 1:
                    ans+= ' ' + e[i][1]
            elif i == "Booktitle":
                ans+= ' ' + e[i][0]
                if len(e[i]) > 1:
                    ans+= ' ' + e[i][1]
            else:
                ans+= ' ' + e[i]
    return ans
    
def level(l):
    a = 0
    for c in l:
        if c == ' ':
            a+=1
        elif c == '\t':
            a+=TAB_VAL
        else:
            break
    return a

def getNames(al):
    ad = al.partition('$')
    if len(ad[2]) == 0:
        return ("",ad[0].strip())
    return (ad[0].strip(),ad[2].strip())

def genCode(a,y):
    gs = ""
    for x in a:
        gs+= ' ' + x[1]
    lgpp = gs.split()
    lgp = []
    for n in lgpp:
        np = n.split('-')
        for e in np:
            if len(e) > 0:
                lgp.append(e)
    lg = []
    for e in lgp:
        if not e[0].islower():
            lg.append(e)
    c = ""
    if len(lg) == 1:
        c = lg[0][0:min(3,len(lg[0]))]
    else:
        for n in lg:
            c+= n[0]
    c+= y[-2:]
    return c

def sortString(e):
    s = ""
    s+= e["Year"]
    if "Month" in e:
        if int(e["Month"]) < 10:
            s+= '0'
        s+= e["Month"]
    else:
        s+= "00"
    if "Day" in e:
        if int(e["Day"]) < 10:
            s+= '0'
        s+= e["Day"]
    else:
        s+= "00"
    n = 0
    while torder[n] != e["Type"]:
        n+=1
    if n < 10:
        s+= '0'
    s+= str(n)
    s+= e["Title"]
    for a in e["Authors"]:
        s+= a[0] + a[1]
    return s

def sortPost(e):
    s = ""
    n = 0
    while torder[n] != e["Type"]:
        n+=1
    if n < 10:
        s+= '0'
    s+= str(n)
    s+= e["Year"]
    if "Month" in e:
        if int(e["Month"]) < 10:
            s+= '0'
        s+= e["Month"]
    else:
        s+= "00"
    if "Day" in e:
        if int(e["Day"]) < 10:
            s+= '0'
        s+= e["Day"]
    else:
        s+= "00"
    s+= e["Title"]
    for a in e["Authors"]:
        s+= a[0] + a[1]
    return s

def indent(lvl):
    s = ""
    for _ in range(lvl*TAB_VAL):
        s+= ' '
    return s
        
def parse(db,fd):
    a = False
    e = False
    n = False
    s = False
    t = ""
    l = 0
    for line in fd:
        if len(line.strip()) > 0:
            if a:
                if level(line) <= l:
                    a = False
                    if "Year" in db[-1] and "Code" not in db[-1]:
                        db[-1]["Code"] = genCode(db[-1]["Authors"],db[-1]["Year"])
                else:
                    db[-1]["Authors"].append(getNames(line))
            elif e:
                if level(line) <= l:
                    e = False
                else:
                    db[-1]["Advisors"].append(getNames(line))
            elif n:
                if level(line) <= l:
                    n = False
                else:
                    if len(db[-1][t]) == 0:
                        db[-1][t]+= line.rstrip()
                    else:
                        db[-1][t]+= ('\n' + line.rstrip())
            elif s:
                if level(line) > l:
                    db[-1][t].append(line.strip())
                s = False
            if not (a or n or s):
                if level(line) == 0:
                    db.append({})
                    tc = line.strip()
                    if tc[0] != '&':
                        db[-1]["Code"] = tc
                else:
                    ln = line.strip().partition(' ')
                    ln0 = ln[0].capitalize()
                    if ln0 == "Authors":
                        a = True
                        l = level(line)
                        db[-1]["Authors"] = []
                    elif ln0 == "Author":
                        db[-1]["Authors"] = [getNames(ln[2])]
                        if "Year" in db[-1] and "Code" not in db[-1]:
                            db[-1]["Code"] = genCode(db[-1]["Authors"],db[-1]["Year"])
                    elif ln0 == "Advisors":
                        e = True
                        l = level(line)
                        db[-1]["Advisors"] = []
                    elif ln0 == "Advisor":
                        db[-1]["Advisor"] = [getNames(ln[2])]
                    elif ln0 == "Year":
                        db[-1]["Year"] = ln[2].strip()
                        if "Authors" in db[-1] and "Code" not in db[-1]:
                            db[-1]["Code"] = genCode(db[-1]["Authors"],db[-1]["Year"])
                    elif ln0 == "Note" or ln0 == "Abstract":
                        n = True
                        t = ln0
                        l = level(line)
                        db[-1][t] = ln[2].strip()
                    elif ln0 == "Type":
                        db[-1]["Type"] = ln[2].strip().lower()
                    elif ln0 == "Keywords":
                        ln2 = ln[2].split("$")
                        db[-1]["Keywords"] = set()
                        for kw in ln2:
                            db[-1]["Keywords"].add(kw.strip())
                    elif ln0 == "Journal":
                        db[-1][ln0] = [ln[2].strip()]
                        s = True
                        t = ln0
                        l = level(line)
                    elif ln0 == "Booktitle":
                        db[-1][ln0] = [ln[2].strip()]
                        s = True
                        t = ln0
                        l = level(line)
                    else:
                        db[-1][ln0] = ln[2].strip()
    return db

def deDupEntry(db,auto):
    rm = []
    if auto:
        for x in range(0,len(db)):
            if x not in rm:
                for y in range(x+1,len(db)):
                    if y not in rm:
                        if (db[x]["Title"] == db[y]["Title"]) and (db[x]["Year"] == db[y]["Year"]):
                            if (db[x]["Type"] == db[y]["Type"]) and (db[x]["Authors"] == db[y]["Authors"]):
                                for key in db[y]:
                                    if key not in db[x]:
                                        db[x][key] = db[y][key]
                                print("Warning! [" + db[x]["Code"] + "][" + db[y]["Code"] + "]-" + db[x]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + " Duplicate removed!")
                                for key in db[y]:
                                    if key not in db[x]:
                                        db[x][key] = db[y][key]
                                rm.append(y)
                            else:
                                print("Caution! [" + db[x]["Code"] + "]-" + db[x]["Type"] + " [" + db[y]["Code"] + "]-" + db[y]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + " Possible duplicate!")
    else:
        for x in range(0,len(db)):
            if x not in rm:
                for y in range(x+1,len(db)):
                    if y not in rm:
                        if (db[x]["Title"] == db[y]["Title"]) and (db[x]["Year"] == db[y]["Year"]):
                            if (db[x]["Type"] == db[y]["Type"]) and (db[x]["Authors"] == db[y]["Authors"]):
                                print("Warning! [" + db[x]["Code"] + "][" + db[y]["Code"] + "]-" + db[x]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + " Duplicate found!")
                            else:
                                print("Caution! [" + db[x]["Code"] + "]-" + db[x]["Type"] + " [" + db[y]["Code"] + "]-" + db[y]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + " Possible duplicate!")
                            inp = ''
                            while(inp != 'y' and inp != 'n'):
                                inp = input("Remove? [y/N]")
                            if inp == 'y':
                                for key in db[y]:
                                    if key not in db[x]:
                                        db[x][key] = db[y][key]
                                rm.append(y)
    rm.sort(reverse=True)
    for x in range(0,len(rm)):
        del db[rm[x]]
    return db

def deDupCode(db):
    kc = set()
    mult = set()
    for e in db:
        c = ""
        if re.fullmatch("(([A-Z]+)|([A-Z][a-z]{0,2}))[0-9]{2}[a-z]",e["Code"]):
            c = e["Code"][0:-1]
        else:
            c = e["Code"]
        e["__OCODE"] = e["Code"]
        if c not in mult:
            if c in kc:
                mult.add(c)
            else:
                kc.add(c)
    for c in mult:
        i = 0
        for e in range(0,len(db)):
            d = ""
            if re.fullmatch("(([A-Z]+)|([A-Z][a-z]{0,2}))[0-9]{2}[a-z]",db[e]["Code"]):
                d = db[e]["Code"][0:-1]
            else:
                d = db[e]["Code"]
            if c == d:
                if db[e]["Code"] != d + string.ascii_lowercase[i]:
                    print("Warning! Code changed! " + db[e]["__OCODE"] + " -> " + d + string.ascii_lowercase[i])
                    db[e]["Code"] = d + string.ascii_lowercase[i]
                i+= 1
    for e in db:
        if "__OCODE" in e:
            del e["__OCODE"]
    return db

def regenCode(db):
    kc = set()
    mult = set()
    for e in db:
        e["__OCODE"] = e["Code"]
        c = genCode(e["Authors"],e["Year"])
        e["Code"] = c
        if c not in mult:
            if c in kc:
                mult.add(c)
            else:
                kc.add(c)
    for c in mult:
        i = 0
        for e in range(0,len(db)):
            d = ""
            if re.fullmatch("(([A-Z]+)|([A-Z][a-z]{0,2}))[0-9]{2}[a-z]",db[e]["Code"]):
                d = db[e]["Code"][0:-1]
            else:
                d = db[e]["Code"]
            if c == d:
                if db[e]["Code"] != d + string.ascii_lowercase[i]:
                    db[e]["Code"] = d + string.ascii_lowercase[i]
                i+= 1
    for e in range(0,len(db)):
        if "__OCODE" in db[e]:
            if db[e]["__OCODE"] != db[e]["Code"]:
                print("Warning! Code changed! " + db[e]["__OCODE"] + " -> " + db[e]["Code"])
            del db[e]["__OCODE"]
    return db

def liner(db,fd):
    db.sort(key=sortPost)
    buff = ''
    for entry in db:
        fd.write(buff + entry["Code"] + '\n')
        for x in korder:
            if x in entry:
                if x == "Authors":
                    a = entry["Authors"]
                    if len(a) == 1:
                        fd.write(indent(1) + "Author " + a[0][0] + " $ " + a[0][1] + '\n')
                    else:
                        fd.write(indent(1) + "Authors\n")
                        for y in a:
                            fd.write(indent(2) + y[0] + " $ " + y[1] + '\n')
                elif x == "Advisors":
                    a = entry["Advisors"]
                    if len(a) == 1:
                        fd.write(indent(1) + "Advisor " + a[0][0] + " $ " + a[0][1] + '\n')
                    else:
                        fd.write(indent(1) + "Advisors\n")
                        for y in a:
                            fd.write(indent(2) + y[0] + " $ " + y[1] + '\n')
                elif x == "Keywords":
                    fd.write(indent(1) + "Keywords ")
                    buff = ''
                    for y in entry["Keywords"]:
                        fd.write(buff + y)
                        buff = " $ "
                    fd.write('\n')
                elif x == "Journal":
                    fd.write(indent(1) + x + ' ' + entry[x][0] + '\n')
                    if len(entry[x]) > 1:
                        fd.write(indent(2) + entry[x][1] + '\n')
                elif x == "Booktitle":
                    fd.write(indent(1) + x + ' ' + entry[x][0] + '\n')
                    if len(entry[x]) > 1:
                        fd.write(indent(2) + entry[x][1] + '\n')
                else:
                    fd.write(indent(1) + x + ' ' + entry[x] + '\n')
        buff = '\n'

def safeLiner(db,outf):
    tout = "."+outf+"._nbb_tmp"
    outp = open(tout,'w')
    try:
        liner(db,outp)
    except:
        os.remove(tout)
    shutil.move(tout,outf)

def output(db,fd):
    db.sort(key=sortPost)
    buff = ''
    for entry in db:
        fd.write(buff + '@' + entry["Type"] + '{' + entry["Code"] + ",\n")
        for x in border:
            if x in entry:
                if x == "Authors":
                    a = entry["Authors"]
                    al = ""
                    for y in a:
                        al+= y[0] + " {" + y[1] + "} and "
                    fd.write((indent(1) + "author").ljust(EQ_LVL*TAB_VAL) + "= \"" + al[0:-5] + "\",\n")
                elif x == "Advisors":
                    a = entry["Advisors"]
                    al = ""
                    for y in a:
                        al+= y[0] + " {" + y[1] + "} and "
                    fd.write((indent(1) + "advisor").ljust(EQ_LVL*TAB_VAL) + "= \"" + al[0:-5] + "\",\n")
                elif x == "Journal":
                    fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][0] + "\",\n")
                elif x == "Booktitle":
                    fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][0] + "\",\n")
                else:
                    fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x] + "\",\n")
        fd.write("}\n")
        buff = '\n'

def outshort(db,fd):
    db.sort(key=sortPost)
    buff = ''
    for entry in db:
        fd.write(buff + '@' + entry["Type"] + '{' + entry["Code"] + ",\n")
        for x in sorder:
            if x in entry:
                if x == "Authors":
                    a = entry["Authors"]
                    al = ""
                    for y in a:
                        al+= y[0] + " {" + y[1] + "} and "
                    fd.write((indent(1) + "author").ljust(EQ_LVL*TAB_VAL) + "= \"" + al[0:-5] + "\",\n")
                elif x == "Advisors":
                    a = entry["Advisors"]
                    al = ""
                    for y in a:
                        al+= y[0] + " {" + y[1] + "} and "
                    fd.write((indent(1) + "advisor").ljust(EQ_LVL*TAB_VAL) + "= \"" + al[0:-5] + "\",\n")
                elif x == "Journal":
                    if len(entry[x]) > 1:
                        fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][1] + "\",\n")
                    else:
                        fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][0] + "\",\n")
                elif x == "Booktitle":
                    if len(entry[x]) > 1:
                        fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][1] + "\",\n")
                    else:
                        fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x][0] + "\",\n")
                else:
                    fd.write((indent(1) + x.lower()).ljust(EQ_LVL*TAB_VAL) + "= \"" + entry[x] + "\",\n")
        fd.write("}\n")
        buff = '\n'
        
def readTex(tf):
    idents = set()
    char = tf.read(1)
    esc = False
    cmt = False
    while char != '':
        if esc:
            esc = False
            if char == 'c':
                char = tf.read(1)
                if char == 'i':
                    char = tf.read(1)
                    if char == 't':
                        char = tf.read(1)
                        if char == 'e':
                            char = tf.read(1)
                            if char == '{':
                                while char != '}':
                                    ident = ''
                                    char = tf.read(1)
                                    while char != '}' and char != ',':
                                        ident+= char
                                        char = tf.read(1)
                                    idents.add(ident)
                                idents.add(ident)
                            elif char == '\\':
                                esc = True
                        elif char == '\\':
                            esc = True
                    elif char == '\\':
                        esc = True
                elif char == '\\':
                    esc = True
            elif char == 'i':
                char = tf.read(1)
                if char == 'n':
                    char = tf.read(1)
                    if char == 'p':
                        char = tf.read(1)
                        if char == 'u':
                            char = tf.read(1)
                            if char == 't':
                                char = tf.read(1)
                                if char == '{':
                                    subfile = ''
                                    char = tf.read(1)
                                    while char != '}':
                                        subfile+= char
                                        char = tf.read(1)
                                    try:
                                        ntf = open(subfile,'r')
                                        idents|= readTex(ntf)
                                    except:
                                        ()
                                elif char == '\\':
                                    esc = True
                            elif char == '\\':
                                esc = True
                        elif char == '\\':
                            esc = True
                    elif char == 'c':
                        char = tf.read(1)
                        if char == 'l':
                            char = tf.read(1)
                            if char == 'u':
                                char = tf.read(1)
                                if char == 'd':
                                    char = tf.read(1)
                                    if char == 'e':
                                        char = tf.read(1)
                                        if char == '{':
                                            subfile = ''
                                            char = tf.read(1)
                                            while char != '}':
                                                subfile+= char
                                                char = tf.read(1)
                                            try:
                                                ntf = open(subfile,'r')
                                                idents|= readTex(ntf)
                                            except:
                                                ()
                                        elif char == '\\':
                                            esc = True
                                    elif char == '\\':
                                        esc = True
                                elif char == '\\':
                                    esc = True
                            elif char == '\\':
                                esc = True
                        elif char == '\\':
                            esc = True
                    elif char == '\\':
                        esc = True
                elif char == '\\':
                    esc = True
        elif cmt:
            if char == '\n':
                cmt = False
        elif char == '%':
            cmt = True
        elif char == '\\':
            esc = True
        char = tf.read(1)
    return idents

def readTexList(tfl):
    idents = set()
    for texf in tfl:
        tf = open(texf,'r')
        idents|= readTex(tf)
    return idents

def filterDb(db,idents):
    ndb = []
    for e in db:
        if e["Code"] in idents:
            ndb.append(e)
            idents.remove(e["Code"])
    if len(idents) > 0:
        if len(idents) > 1:
            print("Warning! The following codes were not found in bibliographic database: ", end='')
            buff = ""
            for c in idents:
                print(buff + c, end='')
                buff = ", "
            print('\n')
        else:
            print("Warning! The following code was not found in bibliographic database: " + idents.pop())
    return ndb

def regen(inl,outf):
    db = []
    for inf in inl:
        inp = open(inf,'r')
        db = parse(db,inp)
    db.sort(key=sortString)
    db = regenCode(deDupEntry(db,False))
    safeLiner(db,outf)

def prod(inf,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    outp = open(outf,'w')
    output(db,outp)

def prodFor(inf,tfl,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = filterDb(deDupCode(deDupEntry(db,False)),readTexList(tfl))
    outp = open(outf,'w')
    output(db,outp)

def short(inf,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    outp = open(outf,'w')
    outshort(db,outp)

def shortFor(inf,tfl,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = filterDb(deDupCode(deDupEntry(db,False)),readTexList(tfl))
    outp = open(outf,'w')
    outshort(db,outp)

def pick(inf,out,fpath):
    if len(fpath) > 0 and fpath[-1] != '/':
        fpath+= '/'
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    if os.path.exists(out):
        shutil.rmtree(out)
    os.makedirs(out)
    for e in db:
        if "File" in e:
            shutil.copy2(fpath+e["File"],out+"/.")

def pickFor(inf,tfl,out,fpath):
    if len(fpath) > 0 and fpath[-1] != '/':
        fpath+= '/'
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = filterDb(deDupCode(deDupEntry(db,False)),readTexList(tfl))
    if os.path.exists(out):
        shutil.rmtree(out)
    os.makedirs(out)
    for e in db:
        if "File" in e:
            shutil.copy2(fpath+e["File"],out+"/.")
    
def read(inf,ident,fpath):
    if len(fpath) > 0 and fpath[-1] != '/':
        fpath+= '/'
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    x = 0
    while x < len(db):
        if db[x]["Code"] == ident:
            if "File" not in db[x]:
                print("Error! Reference found but does not have associated file!")
                return
            break
        x+=1
    lst = []
    if x == len(db):
        x = 0
        while x < len(db):
            if substring(ident,db[x]["Title"]):
                lst.append((x,db[x]["Code"]))
            x+=1
        if len(lst) == 1:
            x = lst[0][0]
    if x < len(db):
        if "File" in db[x]:
            subprocess.run([pdfr,fpath+db[x]["File"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
        else:
            print("Error! Only match found: [" + db[x]["Code"] + "]-" + db[x]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + "does not have associated file!")
    elif len(lst) == 0:
        print("Error! Not found!")
    else:
        print("Possible matches found:")
        af = False
        for y in range(0,len(lst)):
            if "File" in db[y]:
                af = True
                print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"])
            else:
                print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"] + " NO FILE!")
        if af: 
            s = input("Select by code or number, or type anything else to abort")
            if s.isnumeric() and int(s) < len(lst):
                if "File" in db[lst[int(s)][0]]:
                    subprocess.run([pdfr,fpath+db[lst[int(s)][0]]["File"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                else:
                    print("Error! Choice does not have associated file!")
            else:
                for y in lst:
                    if y[1] == s:
                        if "File" in db[y][0]:
                            subprocess.run([pdfr,fpath+db[y[0]]["File"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                        else:
                            print("Error! Choice does not have associated file!")
    
def web(inf,ident):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    x = 0
    while x < len(db):
        if db[x]["Code"] == ident:
            if "Link" not in db[x]:
                print("Error! Reference found but does not have associated link!")
                return
            break
        x+=1
    lst = []
    if x == len(db):
        x = 0
        while x < len(db):
            if substring(ident,db[x]["Title"]):
                lst.append((x,db[x]["Code"]))
            x+=1
        if len(lst) == 1:
            x = lst[0][0]
    if x < len(db):
        if "Link" in db[x]:
            subprocess.run([webb,db[x]["Link"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
        else:
            print("Error! Only match found: [" + db[x]["Code"] + "]-" + db[x]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + "does not have associated link!")
    elif len(lst) == 0:
        print("Error! Not found!")
    else:
        print("Possible matches found:")
        af = False
        for y in range(0,len(lst)):
            if "Link" in db[y]:
                af = True
                print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"])
            else:
                print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"] + " NO LINK!")
        if af: 
            s = input("Select by code or number, or type anything else to abort")
            if s.isnumeric() and int(s) < len(lst):
                if "Link" in db[lst[int(s)][0]]:
                    subprocess.run([webb,db[lst[int(s)][0]]["Link"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                else:
                    print("Error! Choice does not have associated link!")
            else:
                for y in lst:
                    if y[1] == s:
                        if "Link" in db[y[0]]:
                            subprocess.run([webb,db[y[0]]["Link"]],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                        else:
                            print("Error! Choice does not have associated link!")
    
def add(inf,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
    if os.path.exists(TMP_FILE):
        inp = open(TMP_FILE,'r')
        pdb = parse([],inp)
        for c in pdb:
            print("New reference code: "+c["Code"])
        inp.seek(0)
        db = parse(db,inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    safeLiner(db,outf)
    os.remove(TMP_FILE)
    
def remove(inf,ident,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    x = 0
    while x < len(db):
        if db[x]["Code"] == ident:
            break
        x+=1
    lst = []
    if x == len(db):
        x = 0
        while x < len(db):
            if substring(ident,db[x]["Title"]):
                lst.append((x,db[x]["Code"]))
            x+=1
        if len(lst) == 1:
            x = lst[0][0]
    if x < len(db):
        a = ''
        while a != 'y' and a != 'n':
            a = input('[' + db[x]["Code"] + "]-" + db[x]["Type"] + " \"" + db[x]["Title"] + "\" - " + db[x]["Year"] + "\nConfirm deletion?[y/N]")
        if a == 'y':
            del db[x]
    elif len(lst) == 0:
        print("Error! Not found!")
    else:
        print("Possible matches found:")
        for y in range(0,len(lst)):
            print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"])
        s = input("Select by code or number, or type anything else to abort")
        if s.isnumeric() and int(s) < len(lst):
            del db[lst[int(s)][0]]
        else:
            for y in lst:
                if y[1] == s:
                    del db[y[0]]
    safeLiner(db,outf)
    
def edit(inf,ident,outf):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    x = 0
    while x < len(db):
        if db[x]["Code"] == ident:
            break
        x+=1
    lst = []
    if x == len(db):
        x = 0
        while x < len(db):
            if substring(ident,db[x]["Title"]):
                lst.append((x,db[x]["Code"]))
            x+=1
        if len(lst) == 1:
            x = lst[0][0]
    if x < len(db):
        tmpf = open(TMP_FILE,'w')
        liner([db[x]],tmpf)
        tmpf.close()
        del db[x]
        subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
        inp = open(TMP_FILE,'r')
        db = parse(db,inp)
        db.sort(key=sortString)
        os.remove(TMP_FILE)
        db = deDupCode(deDupEntry(db,False))
    elif len(lst) == 0:
        print("Error! Not found!")
    else:
        print("Possible matches found:")
        for y in range(0,len(lst)):
            print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"])
        s = input("Select by code or number, or type anything else to abort")
        if s.isnumeric() and int(s) < len(lst):
            tmpf = open(TMP_FILE,'w')
            liner([db[lst[int(s)][0]]],tmpf)
            tmpf.close()
            del db[lst[int(s)][0]]
            subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
            inp = open(TMP_FILE,'r')
            db = parse(db,inp)
            db.sort(key=sortString)
            os.remove(TMP_FILE)
            db = deDupCode(deDupEntry(db,False))
        else:
            for y in lst:
                if y[1] == s:
                    tmpf = open(TMP_FILE,'w')
                    liner([db[y[0]]],tmpf)
                    tmpf.close()
                    del db[y[0]]
                    subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                    inp = open(TMP_FILE,'r')
                    db = parse(db,inp)
                    db.sort(key=sortString)
                    os.remove(TMP_FILE)
                    db = deDupCode(deDupEntry(db,False))
    safeLiner(db,outf)

def view(inf,ident):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    x = 0
    while x < len(db):
        if db[x]["Code"] == ident:
            break
        x+=1
    lst = []
    if x == len(db):
        x = 0
        while x < len(db):
            if substring(ident,db[x]["Title"]):
                lst.append((x,db[x]["Code"]))
            x+=1
        if len(lst) == 1:
            x = lst[0][0]
    if x < len(db):
        tmpf = open(TMP_FILE,'w')
        liner([db[x]],tmpf)
        tmpf.close()
        del db[x]
        subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
        os.remove(TMP_FILE)
    elif len(lst) == 0:
        print("Error! Not found!")
    else:
        print("Possible matches found:")
        for y in range(0,len(lst)):
            print(str(y) + ": [" + lst[y][1] + "]-" + db[lst[y][0]]["Type"] + " \"" + db[lst[y][0]]["Title"] + "\" - " + db[lst[y][0]]["Year"])
        s = input("Select by code or number, or type anything else to abort")
        if s.isnumeric() and int(s) < len(lst):
            tmpf = open(TMP_FILE,'w')
            liner([db[lst[int(s)][0]]],tmpf)
            tmpf.close()
            del db[lst[int(s)][0]]
            subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
            os.remove(TMP_FILE)
        else:
            for y in lst:
                if y[1] == s:
                    tmpf = open(TMP_FILE,'w')
                    liner([db[y[0]]],tmpf)
                    tmpf.close()
                    del db[y[0]]
                    subprocess.run([tedit,TMP_FILE],stdout=open("/dev/null",'w'),stderr=open("/dev/null",'w'))
                    os.remove(TMP_FILE)
    
def lookup(inf,ident):
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    for x in db:
        if x["Code"] == ident:
            print('[' + x["Code"] + "]-" + x["Type"] + " \"" + x["Title"] + "\" - " + x["Year"])
            return
    print("Error! Not found!")

def search(inf,mode,expr,full):
    srch = []
    if mode == '*':
        srch = korder
    elif mode == '+':
        srch = skeys
    else:
        for c in mode:
            if c == 'a':
                srch.append("Authors")
            elif c == 'b':
                srch.append("Booktitle")
            elif c == 'c':
                srch.append("Code")
            elif c == 'e':
                srch.append("Advisors")
            elif c == 'f':
                srch.append("File")
            elif c == 'j':
                srch.append("Journal")
            elif c == 'k':
                srch.append("Keywords")
            elif c == 'l':
                srch.append("Link")
            elif c == 'n':
                srch.append("Note")
            elif c == 'r':
                srch.append("Abstract")
            elif c == 's':
                srch.append("School")
            elif c == 't':
                srch.append("Title")
            elif c == 'u':
                srch.append("Type")
            elif c == 'y':
                srch.append("Year")
    inp = open(inf,'r')
    db = parse([],inp)
    db.sort(key=sortString)
    db = deDupCode(deDupEntry(db,False))
    lst = []
    if full:
        for x in db:
            if searchAll(expr,prepSearch(x,srch)):
                lst.append(x)
        if len(lst) > 0:
            print("Matches found:")
            for x in lst:
                print('[' + x["Code"] + "]-" + x["Type"] + " \"" + x["Title"] + "\" - " + x["Year"])
    else:
        for x in db:
            s = searchOne(expr,prepSearch(x,srch))
            if s > 0:
                lst.append((x,s))
        lst.sort(key=lambda x: x[1],reverse=True)
        if len(lst) > 0:
            print("Matches found:")
            for x in lst:
                print('[' + x[0]["Code"] + "]-" + x[0]["Type"] + " \"" + x[0]["Title"] + "\" - " + x[0]["Year"] + " |" + str(x[1]))
    if len(lst) == 0:
        print("No match")

def argCount(n):
    if len(sys.argv) <= n:
        print("Error: Incorrect argument count!")
        sys.exit(1)


argCount(2)
func = sys.argv[1]
if os.path.isfile(CFG_FILE):
    with open(CFG_FILE,'r') as cf:
        cff = cf.read()
    pff = tomlkit.parse(cff)
elif os.path.isfile(os.path.expanduser("~/"+CFG_FILE)):
    with open(os.path.expanduser("~/"+CFG_FILE),'r') as cf:
        cff = cf.read()
    pff = tomlkit.parse(cff)
else:
    pff = {}

torder = TYPE_ORDER
korder = KEY_ORDER
border = BIB_ORDER
sorder = SHORT_ORDER
skeys = SEARCH_KEYS
tedit = EDIT_CMD
pdfr = PDF_CMD
webb = WEB_CMD
ddb = DEF_DB
dpath = DEF_PATH

if "text_edit" in pff:
    tedit = pff["text_edit"]
if "pdf_view" in pff:
    pdfr = pff["pdf_view"]
if "web_b" in pff:
    webb = pff["web_b"]
if "type_order" in pff:
    torder = pff["type_order"]
if "key_order" in pff:
    korder = pff["key_order"]
if "bib_order" in pff:
    border = pff["bib_order"]
if "short_order" in pff:
    sorder = pff["short_order"]
if "search_keys" in pff:
    skeys = pff["search_keys"]
if "def_db" in pff:
    ddb = pff["def_db"]
if "files_path" in pff:
    dpath = pff["files_path"]

if sys.argv[2] != "_":
    ddb = sys.argv[2]
    

if func == "prod":
    argCount(3)
    if len(sys.argv) < 5:
        prod(ddb,sys.argv[3])
    else:
        prodFor(ddb,sys.argv[3:-1],sys.argv[-1])
elif func == "short":
    argCount(3)
    if len(sys.argv) < 5:
        short(ddb,sys.argv[3])
    else:
        shortFor(ddb,sys.argv[3:-1],sys.argv[-1])
elif func == "regen":
    argCount(2)
    if len(sys.argv) < 4:
        regen([ddb],ddb)
    elif len(sys.argv) < 5:
        regen([ddb],sys.argv[-1])
    else:
        regen([ddb]+sys.argv[3:-1],sys.argv[-1])
elif func == "add":
    argCount(2)
    if len(sys.argv) < 4:
        add(ddb,ddb)
    else:
        add(ddb,sys.argv[3])
elif func == "del":
    argCount(3)
    if len(sys.argv) < 5:
        remove(ddb,sys.argv[3],ddb)
    else:
        remove(ddb,sys.argv[3],sys.argv[4])
elif func == "edit":
    argCount(3)
    if len(sys.argv) < 5:
        edit(ddb,sys.argv[3],ddb)
    else:
        edit(ddb,sys.argv[3],sys.argv[4])
elif func == "view":
    argCount(3)
    view(ddb,sys.argv[3])
elif func == "read":
    argCount(3)
    if len(sys.argv) < 5:
        read(ddb,sys.argv[3],dpath)
    else:
        read(ddb,sys.argv[3],sys.argv[4])
elif func == "web":
    argCount(3)
    web(ddb,sys.argv[3])
elif func == "find":
    argCount(3)
    lookup(ddb,sys.argv[3])
elif func == "pick":
    argCount(4)
    if os.path.isfile(sys.argv[-2]):
        pickFor(ddb,sys.argv[3:-1],sys.argv[-1],dpath)
    else:
        pickFor(ddb,sys.argv[3:-2],sys.argv[-1],sys.argv[-2])
elif func == "pick-all":
    argCount(3)
    if len(sys.argv) < 5:
        pick(ddb,sys.argv[3],dpath)
    else:
        pick(ddb,sys.argv[4],sys.argv[3])
elif func == "search":
    argCount(3)
    if len(sys.argv) < 5:
        search(ddb,'+',sys.argv[3],True)
    else:
        search(ddb,sys.argv[3],sys.argv[4],True)
elif func == "search1":
    argCount(3)
    if len(sys.argv) < 5:
        search(ddb,'+',sys.argv[3],False)
    else:
        search(ddb,sys.argv[3],sys.argv[4],False)
else:
    print("Error! Function \""+func+"\" is not valid!")

# General structure
A bibliographic database is a (UTF-8) text file consisting of several *entries* separated by and empty line.

The structure of the file is given by the off-side rule. Indentation **must** rely **only** on **spaces** (no tabulations). One level of indentation is exactly **4** spaces, nothing else.

# Entry
An entry is declared on level 0 of indentation by its **code** (see below) and contains several **fields**, which are declared by their name on level 1 of indentation. If you do not want to worry about the right code to use, just put `&` as a placeholder, NeoBib will generate the code for you.

## Code
The code of an entry is the first letter of the last names of its authors followed by the last two digits of the year. In case of duplicate, as letter is added at the end of the code following an order given by the type of reference (see *README.md*), the title and the names of authors. NeoBib can manage duplicates itself and propose new codes when duplicates are found.

If an author has several last names, all are taken. If an author does not have a last name, their full name is taken as last name. Last names must start with an uppercase letter or they are ignored. If there is only one author the first three letters of their last name are taken (lower case except for the first; if the name is shorter, it is taken fully).

For example (`$` separates first and last names), a paper of 2021 from `Jane $ Smith, John Joseph $ Martin, Barack $ Lincoln Kennedy, Bertha $ von Helmholtz and Heathcliff` will have `SMLKHH21` as code.

## Fields
You may use any field name from BibTeX, at least as long as it is declared in your configuration (see *README.md*). The only different is that, with NeoBib, fields' names start with an uppercase letter. 

The field name is always one single word, all the other words of the line are the value of the field.

For example, `Title My super paper` declares the current reference's title as `My super paper`.

## Special fields
The following fields have some particularities.

### *Type*
This field declares the type of reference as one of the usual BibTeX types (unpublished, article, book, …).

### *Author* and *Authors*
The field *Author* is to be used for a single author. `$` is used as separator for first and last names (any middle name is to be put after the first name and before the only `$` separator; if there is only one name, put it without separator). For example: `Jane $ Smith`, `Franklin D. $ Roosevelt`. Do not use `first {last}` as in BibTeX (NeoBib will translate).

If there are several authors, use the field *Authors* and put each name on a separate line, starting at level 2 of indentation.

### *Advisor* and *Advisors*
Same as for *Author(s)*.

### *Editor*
Nothing particular for NeoBib but note that you may use BibTeX's usual syntax for name: `Jane {Smith} and John Joseph {Martin}` (NeoBib's syntax will not work).

### *Journal* and *Booktitle*
You may put an additional line, starting at level 2 of indentation, to declare a short name for a venue, for example `NIPS'17` instead of `Advances in Neural Information Processing Systems 30`.

### *Keywords*
Use `$` to separate keywords (may include spaces).

### *Abstract*
Can be written on several lines. The first line may be the same as declaration on the next one but all lines except for the declaration line must starting at level 2 of indentation.

### *Note*
The value of this field is a personal note on a reference. It can be written the same way as *Abstract*.

# Syntax highlight
A syntax highlight definition file for [KTextEditor](https://api.kde.org/frameworks/ktexteditor/html/) is provided as `neobib.xml`. You can use it with the fantastic [Kate](https://kate-editor.org/).
